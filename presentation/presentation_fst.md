# 概述

+ 基于seL4的进程控制组机制
    - 限制资源使用
    - 隔离程序

+ 意义
    - 运行不可靠的程序不会耗光系统资源
    - 运行不可靠的程序不会影响其他程序

# 背景

+ seL4内核

    * 第一个完全形式化验证的操作系统内核
    * 安全性：代码注入、缓冲区溢出、内存泄漏
    * IPC：高性能、低延迟 ^[http://l4hq.org/docs/performance.php]
    * 微内核：Server-Client模式

+ Control Group

    * Linux内核提供的一种可以限制、记录、隔离进程组所使用的资源（CPU、Memory、IO）的机制
    * Docker、Libcontainer的基础

# 架构

![进程控制组的层级结构 \label{fig:control_cgroup_hierarchy}](images/control_cgroup_hierarchy.png)

# 实现 \quad 分组管理

+ 分组管理的功能由进程控制组服务进程

+ 在进程控制组服务进程中注册信息

    + 创建控制组
    + 控制组中创建任务进程

+ 进程控制组守护进程

    + 负责监视容器中的进程的状态
    + 管理IPC
    + 管理任务进程的运行（超时控制等）

# 实现 \quad 资源限制

+ CPU
    - 配置控制组中任务进程的优先级
    - 周期性地暂停任务进程
+ 内存
    - seL4的Page Fault会交给用户态程序处理
    - 控制组中的程序只能通过特定的API获取内存
    - 进程控制组服务进程负责处理Page Fault，限制程序超额申请内存
+ 硬件IO
    - seL4的驱动工作在用户态
    - 进程控制组服务进程通过设置IRQHandler接管中断

# 实现 \quad 隔离

\begincols{}

\column{0.34\textwidth}

+ 隔离机制
    * 不可见
    * 不可通信
+ Linux：namespace机制
+ 实现思路：隔离IPC
    * CGroup Server负责转发和控制

\column{0.65\textwidth}

![IPC的隔离 \label{fig:control_cgroup_isolate}](images/control_cgroup_isolate.png)

\stopcols

# 实现 \quad 安全性

+ 进程控制组机制的安全性
    1. 不会对主机造成影响
    2. 不会对其他控制组里的程序造成影响

+ 基于seL4的进程控制组机制
    1. seL4内核本身是安全的
    2. 控制了进程的IPC $\Longrightarrow$ 控制了进程之间的影响

# 实现 \quad API封装

+ Linux: 子系统及控制组 $\Longleftrightarrow$ VFS目录
    - 读写文件夹 $\Longrightarrow$ 操作CGrouop
        + 配置：echo 102400 > memory.max_usage_in_bytes
        + 运行任务：cgclassify -g _group_ _pid_
+ seL4: 无VFS
    - libcg ^[http://libcg.sourceforge.net/html/index.html] 风格API: 对CGroup文件系统的抽象
        + 配置：cgroup_create_cgroup、cgroup_add_controller
        + 运行任务：cgroup_attach_task、cgroup_attach_task_pid

# 难点

1. 正确性
    + 资源限制部分的实现
    + 解决：覆盖测试
2. 性能
    + 对IPC的隔离不至于引起太大的性能开销
    + Benchmark

# 预估工作量

* 需要设计和实现的模块

    + 进程加载和分组管理
    + 资源的限制和管理
    + IPC的隔离
    + libcg风格的API封装
    + 测试、Benchmark和Demo

# 时间安排

: 进度安排表 \label{tab:project-schedule}

---------------   ---------------------------------
2016.10-2016.12   调研
2017.01-2017.02   完成进程组管理机制
2017.02-2017.03   完成资源配置与管理机制
2017.03-2017.04   API封装、测试及Benchmark
2017.04-2017.05   应用功能展示
---------------   ---------------------------------
