# 概述

CGroup是Linux的资源分组管理和进程控制机制

+ 静态、动态配置
+ 进程分组管理
+ IPC
+ 进程资源控制（内存、CPU、IO）

# 整体结构

![进程控制组的结构 \label{fig:overview_hierarchy}](images/overview_hierarchy.png)

# 配置

+ 静态配置：通过配置文件生成C文件，每一个Config Entry包含：

    - 资源（内存、CPU）
    - IPC端点名声明
    - 需要与之通信的IPC端点名

![构建流程 \label{fig:configuration_workflow}](images/configuration_workflow.png)

# IPC管理

+ IPC管理：借鉴远程服务中的Service Qualifier（服务名）的思路，每个进程可以在配置
  声明一个服务，其他进程在配置中声明自己需要用到的服务的名字的列表

+ 启动进程时IPC相关的流程

    1. 遍历配置文件得到所有进程声明的服务名，为每个服务名（对应的进程）创建一个端点
    2. 对每个进程，将其用到的服务名对应的端点（权能）拷贝给该进程

# IPC管理

![IPC隔离和控制 \label{fig:ipc_endpoint}](images/ipc_endpoint.png)

# CPU配额

用户态调度器

+ 进程控制组中的进程优先级小于控制组进程
+ 在控制组进程中设置好Timer，每次收到Timer的异步通知时执行调度

用户态调度原则

+ 任何进程占用的时间片数量都不能超出配置
+ 首先调度优先级高的进程
+ 同一优先级，调度剩余配额百分比最大的进程
+ 无其他进程可调度时，执行idle进程。

# CPU配额

用户态调度算法

+ 算法：优先队列

+ 比较键值：优先级 + 剩余配额

: 复杂度分析 \label{tab:schedule-algo-complexity}

操作                        时间复杂度
-------------------------   ------------------
初始化                      $O(n\log{n})$
找到下一个进行              $O(1)$
将进程放入等待队列          $O(\log{n})$
-------------------------   ------------------

# 内存管理

![内存分组管理机制示意图 \label{fig:memory_manage}](images/memory_manage.png)

# 内存资源

+ 栈空间：默认栈大小：64KB（构建时确定，`CONFIG_SEL4UTILS_STACK_SIZE`）

+ 堆内存：创建进程时，由进程控制组将Untyped Memory的Cap拷贝给子进程

+ libmuslc malloc：分两种情况（是否设置了`CONFIG_MUSLC_SYS_MORECORE_BYTES`）
 
    - 是：使用大小为`CONFIG_MUSLC_SYS_MORECORE_BYTES`的值的静态数组作为malloc所能使用
      的内存区域
    - 否：进程需要显示地从自己的Untyped Memory中分出一部分，设置为malloc所能使用的内存区域
      (设置`muslc_brk_reservation`变量)

# 进一步工作计划

+ IOPort 管理（对应于CGroup的blkio）
+ libcgroup API
+ 测试、Benchmark、demo。

# 时间安排

: 进度安排表 \label{tab:project-schedule}

  时间            任务                         完成情况
---------------   --------------------------   -------------------------------------------
2016.10-2016.12   调研                         $\text{\rlap{$\checkmark$}}\square$
2017.01-2017.02   完成进程组管理机制           $\text{\rlap{$\checkmark$}}\square$
2017.02-2017.03   完成资源配置与管理机制       $\text{\rlap{$\checkmark$}}\square$（部分）
2017.03-2017.04   API封装、测试及Benchmark     $\text{\rlap{}}\square$
2017.04-2017.05   应用功能展示                 $\text{\rlap{}}\square$
---------------   --------------------------   -------------------------------------------


