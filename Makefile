## Makefile to build pdf documents.

PANDOC					:= pandoc
PANDOC_OPTS 			:= -s --latex-engine=xelatex \
						--latex-engine-opt=-shell-escape \
						--natbib --bibliography reference/reference.bib \
						--csl "reference/acm-sig.csl" \
						--filter pandoc-citeproc \
						--number-sections \
						--listings --filter templates/pandoc-minted.py
						## pandoc-crossref

BIBTEX_CMD 				:= bibtex
XELATEX_CMD				:= xelatex "-halt-on-error" "-interaction" "nonstopmode" "-shell-escape"

ARTICLE					:= meta.txt \
						   article/article.md \
						   article/related.md \
						   article/reading.md \
						   article/cgroup.md \
						   article/motivation.md \
						   article/design.md \
						   article/implement.md \
						   article/conclusion.md

CGROUP_REPORT           := meta-cgroup.txt article/cgroup.md

PROPOSAL				:= meta-proposal.txt article/proposal.md

PRESENTATION			:= meta-presentation.txt $(wildcard presentation/*.md)

ARTICLE_TPL				:= --template=templates/pandoc-article.tex
PRESENTATION_TPL		:= --template=templates/pandoc-presentation.tex

all: presentation.pdf proposal.pdf

presentation.pdf: presentation.tex
	$(XELATEX_CMD) $<
	$(XELATEX_CMD) $<
	$(XELATEX_CMD) $<

%.pdf: %.tex
	$(XELATEX_CMD) $<
	$(BIBTEX_CMD) $(basename $@)
	$(XELATEX_CMD) $<
	$(XELATEX_CMD) $<

article.tex: $(META) $(ARTICLE)
	$(PANDOC) $(ARTICLE) -o "article.tex" $(PANDOC_OPTS) $(ARTICLE_TPL)

proposal.tex: $(META) $(ARTICLE)
	$(PANDOC) $(PROPOSAL) -o "proposal.tex" $(PANDOC_OPTS) $(ARTICLE_TPL)

cgroup_report.tex: $(META) $(CGROUP_REPORT)
	$(PANDOC) $(CGROUP_REPORT) -o "cgroup_report.tex" $(PANDOC_OPTS) $(ARTICLE_TPL)

presentation.tex: $(META) $(PRESENTATION)
	$(PANDOC) $(PRESENTATION) -o "presentation.tex" -t beamer $(PANDOC_OPTS) $(PRESENTATION_TPL)

clean:
	$(RM) -f *.aux *.log *.out *.pyg *.toc *.xdv *.nav *.snm *.bbl *.blg
	$(RM) -f article.tex proposal.tex cgroup_report.tex presentation.tex
.PHONY: clean

