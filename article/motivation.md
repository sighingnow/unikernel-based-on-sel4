Motivation
==========

+ 传统内核的理念在个人PC不断普及、云服务、嵌入式应用等背景下的局限性。
+ Unikernel的有点。
    - 通过Hypervisor解决硬件兼容性问题。
    - 特化程度高，资源利用效率高。
    - 启动快，高可扩展性。
    - 冗余少，更加安全。

Containers are a solution to the problem of how to get software to run reliably when moved from one computing environment to another.

The need for container
----------------------

### Comparation

+ 概念
    - VM：Hardware Virtualization, full virtualization mechanism emulates the hardware.
    - Container：Operation System Virtualization, lightweight virtual environment that groups
      and isolateds a set of processes and resources(memory, cpu, disk, etc.) from the host.
      **Guarantee**: any processes inside the container cannot see any processes or resources outside
      the container.
+ 特点
    - VM：多个独立的Kernel
    - Container：同一个Kernel上运行多个独立的用户空间。
+ Isolation:
    - VM: full isolation.
    - Container: the isolation between the host and the container is not as strong as hypervisor-based
      virtualization since all containers share the same kernel of the host 

