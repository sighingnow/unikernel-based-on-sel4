Unikernel
=========

## Unikernels: Library Operating Systems for the Cloud

ASPLOS 2013[@madhavapeddy_unikernels:_2013_conferencepaper]

### Introduction

1. 传统技术与Unikernel的对比:
    + VM：功能完整，特化程度小。(a self-contained computer, little specialisation.)
    + libOS：系统本省直接操作硬件资源，难以适配不同的硬件环境。
    + Unikernel：单用途的“器具”，(single-purpose appliances)，在编译期特化。
2. MirageOS
    + Library for systems programming in OCaml.
    + OCaml: Ocaml能够提供类型安全的保障，并且不造成性能损失(type safety without demaging performance)。
3. MirageOS的定位
    + 以Hypervisor作为目标运行平台，通过Hypervisor来解决硬件兼容性问题。
    + 强调云服务，(address cloud services rather than desktop applications)。
    + 高度组件化，通过Library的形式来提供系统模块，例如网络、存储、等等。
4. 兼容性
    + 无法将已经有的应用程序直接通过MirageOS构建成Unikernel，通过牺牲源代码的向后兼容性来获得性能提升。
    + 与外部环境通过TCP/IP等标准协议来实现兼容，不支持POSIX API。

### Architecure

#### Mirage OS的体系结构

MirageOS的基本思想如图\ref{fig:mirage-os-arch}

![Mirage OS \label{fig:mirage-os-arch}](images/mirage-os-arch.png)

#### 优势

1. 最终构建得到的Appliance高度压缩，体积小
    + 节省资源，有利于通过Internet进行云服务的快速部署。
    + 启动快。
2. 安全性提高。
    + 冗余小，攻击面(attack surface)小。
    + 使用Ocaml实现，具有类型安全的优势，保证系统是一个Well-typed程序，在语言层面不存在未定义行为。
    + Hypervisor：假设Hypervisor是安全的。

#### 实现要点

1. 硬件资源：Hypervisor(Xen)提供基本的硬件资源的抽象。
2. 操作系统所需的模块：所有服务都以库的形式实现，例如调度器、设备驱动、网络栈。
3. 构建：在构建过程中通过configuration来确定应当将哪些组件整合进最终的Kernel。
    + 传统的配置文件：ad-hoc text files(ini, conf, etc.)，导致维护困难，容易出错且不容易排错。
    + MirageOS：**programmable** configuration。
    + 编译器使用SAT Solver计算依赖是否可以满足。
4. 选择Ocaml：
    + 类型安全
    + 利用Ocaml的**Module**机制来实现不同组件之间的逻辑隔离。
    + 不支持Ocaml以外的其他编程语言：对于一个single-purpose的内核来说，有利于减小不同编程语言的运行时带来的复杂性。
      同时，使用单一的高级语言更容易静态地分析整个系统的行为(easy to reason about)，有利于减少BUG。
5. 不同的Unikernel之间的隔离：使用Hypervisor(Xen)提供的硬件抽象和资源隔离。
6. 数据交换：
    + SSL/TLS
    + 共享内存
    + vchan：fast shared memory channel。
7. **单进程**，无需进程调度。
8. **单地址空间**，无传统的内核空间和用户空间的区别。页表采用不可变数据结构，当Unikernel启动时，就根据编译期的配置
   进行预分配(PVBoot)。
    + Zero-copy IO。

#### Related Work

1. Jitsu[@madhavapeddy_jitsu:_2015_conferencepaper]：基于MirageOS 2.0的减小网络服务延迟的方案。
2. Mirage OS已经在实际的商业云服务环境中部署实用，但仍需要更丰富的Protocol Derivers。

## Unikernels: Rise of the Virtual Library Operating System

ACM Queue[@madhavapeddy_unikernels:_2013_journalarticle]

这篇文档使MirageOS项目的作者Anil Madhavapeddy发表在ACM Queue上的一篇综述性文章，主要内容包括：

1. 传统的分时操作系统在大规模云服务环境下和嵌入式设备中存在的问题
    + 多用户机制使得操作系统的复杂性提高，容易出BUG，而如今Hardware is cheap。
    + 传统操作系统针对硬件的兼容能力广，但过于冗余，没有必要的驱动（如打印机驱动）可能存在的漏洞会对系统的安全构成威胁。
    + 嵌入式设备：一般是一个Single-app System，不需要支持多用户。
    + VM：优化程度不够，Unikernel可以针对单个应用进程针对性的优化。
    + 传统的内核间接抽象层复杂，对于一个简单的应用，call path也会非常复杂，（Every problem can be solved by
      **removing** layers of indirection）。
2. Unikernel
    + container + more security and isolation。
3. Logical Workflow of MirageOS如图 \ref{fig:mirageos-workflow}

![MirageOS Workflow \label{fig:mirageos-workflow}](images/mirageos-workflow.png)

## IncludeOS A minimal, Resource Efficient Unikernel for Cloud Services

ICCTS 2015[@bratterud_includeos:_2015_conferencepaper]

### Introduction

IncludeOS是一个使用C++实现的Unikernel，强调极小的Binary size和Memory footprint。

### 实现要点

1. 资源利用效率高，(When idle, it uses no CPU at all)。
2. 构建和部署都非常高效。

        #include <os>

3. 依赖于虚拟化平台（VirtualBox），可以通过Web直接部署上在OpenStack上。
4. 模块化
    + 每一个模块编译到一个**目标文件**(`ip4.o`, `udp.o`, etc.)，使用`ar`打包，在最终链接时就只会将用到的部分编译进内核。
    + 无冗余，减小额外开销。
    + 使用代理(Delegates)作为模块与模块之间的连接。
5. C/C++标准库：
    + 使用newlib作为C标准库以减少对POSIX系统调用的依赖。
    + 使用EASTL，禁用异常。
6. IO：
    + Driver: virtio + VirtioNet。
    + 异步IO，callback-based programming。
    + deferred IRQ，消除上下文切换与数据竞争。

## Rumprun

Rumprun：kernel-based unikernel，是一个可兼容现有的POSIX应用程序的Unikernel实现。目标是提供kernel-quality的组件，可以作为
普通的操作系统内核与Unikernel appliance之间的一个简介抽象层 ^[[On rump kernels and the Rumprun unikernel](https://blog.xenproject.org/2015/08/06/on­rump­kernels­and­the­rumprun­unikernel/)]。Rumprun的系统结构如图 \ref{fig:rumprun-arch}：

![Rumprun \label{fig:rumprun-arch}](images/rumprun-arch.png)

The seL4 Microkernel 
====================


Genode OS Framework
===================





