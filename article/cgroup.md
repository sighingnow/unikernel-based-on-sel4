cgroup
======

基本构成
--------

cgroup（control group）是Linux内核提供的一种隔离进程组，限制进程的物理资源使用的机制。cgroup使LXC和Docker的基础。
cgroup将系统的硬件资源分为数个子系统，包括`blkio`、`cpu`和`memory`等等。可以限制的资源包括

+ 资源的使用数量：当进程组中的进程使用的内存超过子系统设定的限制时，程序无法继续获得内存，会被挂起或者停止。
+ 资源的使用优先级：通过设置进程组的cpu share的值来控制操作系统的调度算法对该进程组中的进程的调度，从而实现控制进程
的CPU使用率。
+ 硬件设备的使用：`blkio`子系统可以用来设置对于一个进程组能够使用IO设备的最大速率，`devices`子系统可以用来设置是否
启用某个具体的设备。

Linux上cgroup的全部信息以虚拟目录的方式表达，`/sys/fs/cgroup/`目录下（Ubuntu 16.04系统）包含的每个
文件夹都是系统能够支持的一个子系统。每个目录下包含数个虚拟文件，用于配置cgroup。例如`memory`目录下的
`memory.limit_in_bytes`可以用来控制进程组中进程的最大内存使用，`memory.oom_control`可以用来配置当进程
申请的内存数量超过限制时挂起程序还是直接杀死进程。

层级系统
--------

cgroup的层级结构也表达为子目录结构，可以使用`mount/unmount`命令改变`cpu`、`memory`等子系统的挂载层级，例如，
将`cpu`和`memory`挂载在同一个目录上，相当于创建了一个可以同时对CPU和内存的使用进行控制的自定义子系统。
进程控制组（control group）相当于在子系统的目录下创建的子目录，通过目录中的各种虚拟文件可以改变进程组的
参数。例如，在`/sys/fs/cgroup/memory`目录下创建一个子目录，就相当于创建了一个可以用来控制内存使用的进程
控制组。

cgroup中，每个task都是系统的一个进程，同一个进程可以同时在多个进程组中，可以对进程可以使用的设备、CPU、内存
等多种资源进行限制。

使用
----

创建、更改和设置子系统与控制组的参数，可以通过修改系统的配置文件（`/etc/cgconfig.conf`）进行全局设置，
也可以通过直接操作`/sys/fs/cgroup/`文件夹内的内容来实现，还是用使用`cgroup-bin`包中提供的`cgcreate`、`cgdelete`
等命令行工具进行。可以使用`cgexec`命令在指定的进程控制组中（一个或多个）运行执行的程序，从而实现对程序的资源
占用的限制。

libcgroup
=========

libcgroup是一个cgroup上层的库，封装了对`cgroup`的虚拟目录的操作，可以用于在程序中创建、修改和删除控制组，
以及在执行的控制组中运行进程（task）。libcgroup将内存、CPU、设备等抽象成为`controller`，一个`group`可以拥
有多个`controller`，通过`cgroup_add_value_*`、`cgroup_set_value_*`、`cgroup_get_value_*`可以修改对控制组的
配置，以及获取当前的配置信息。通过`cgroup_attach_task`和`cgroup_attach_task_pid`等函数可以将指定的进程加入
到执行的控制组中运行。

使用
----

使用libcgroup操作cgroup的简单demo：

~~~c
int main() {
    char *argv = {NULL};

    if ((ret = cgroup_init()) != 0) {
        exit(0);
    }

    g = cgroup_new_cgroup("cglearn");
    ctrl = cgroup_add_controller(g, "memory");
    cgroup_set_value_uint64(ctrl, "memory.limit_in_bytes", 10240000);

    cgroup_create_cgroup(g, 0);

    cgroup_attach_task(g);
    execvp("./allocate-fifty-mb.out", argv);

    return 0;
}
~~~

Namespace
=========

Namespace机制为Namespace中的进程提供设定的虚拟运行环境（context），每个Namespace对于其他的Namespace都是透明的。
例如，在一个Namespace中运行`ps`命令只能看到当前Namespace中的进程，Linux的所有进程都在一个根命名空间中。使用举例：

```shell
unshare --mount --ipc --pid --mount-proc=/proc --fork su -l ${SUDO_USER}
```

将创建一个隔离PID的命名控件，在其中执行`ps`命令只能看到Namespace中的进程的信息，同一个进程的PID与在Namespace的上层
看到的也不相同，说明确实起到了隔离的作用。

Linux的namespace包括六个部分：

+ Mount
+ PID
+ IPC
+ UTS：hostname and domain name
+ Network
+ User

调用`setns`函数可以将一个进程加入到指定的namespace中（namespace以文件描述符的方式表示）。

可行性
======

+ 目标：实现cgroup的进程组管理、CPU和Memory的限制、并提供类似于libcgroup风格的API。
+ 实现：seL4对于有Capability的概念，同样具有层级结构，限制内存占用的实现难度不难，对CPU资源的限制可以对调度进行
进行改动来实现。至于隔离部分，Ubuntu的cgroup的ns(namespace)子系统已经被移除（资源隔离会给cgroup带来诸多问题：命名
冲突、逃逸）。**以及，我仍然不太确定隔离机制本身到底意味着什么，以及做到什么程度才算是真正的实现了隔离？**
