LXC与Docker
===========

Docker的应用场景 ^[[八个Docker的真实应用场景](http://dockone.io/article/126)]：

1. 简化配置
2. 代码流水线（Code Pipeline）管理
3. 提高开发效率
4. 隔离应用
5. 整合服务器
6. 调试能力Docker
7. 多租户环境
8. 快速部署

